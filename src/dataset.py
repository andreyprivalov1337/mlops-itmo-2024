import os
import click
import pandas as pd


@click.command()
@click.option(
    "--data-dir",
    type=click.Path(exists=True),
    default="../data/raw",
    help="Directory containing raw data files.",
)
@click.option(
    "--output-dir",
    type=click.Path(),
    default="../data/interim",
    help="Directory to save the merged data file.",
)
def main(data_dir, output_dir):
    contract_path = os.path.join(data_dir, "contract.csv")
    internet_path = os.path.join(data_dir, "internet.csv")
    personal_path = os.path.join(data_dir, "personal.csv")
    phone_path = os.path.join(data_dir, "phone.csv")

    contract = pd.read_csv(contract_path, index_col="customerID")
    personal = pd.read_csv(personal_path, index_col="customerID")
    internet = pd.read_csv(internet_path, index_col="customerID")
    phone = pd.read_csv(phone_path, index_col="customerID")

    def merge_data(contract, personal, internet, phone):
        final = (
            contract.merge(personal, how="left", on="customerID")
            .merge(internet, how="left", on="customerID")
            .merge(phone, how="left", on="customerID")
        )
        return final

    final_data = merge_data(contract, personal, internet, phone)

    os.makedirs(output_dir, exist_ok=True)
    output_file = os.path.join(output_dir, "final.csv")
    final_data.to_csv(output_file, index=True)
    click.echo(f"Data merged and saved to {output_file}")


if __name__ == "__main__":
    main()
