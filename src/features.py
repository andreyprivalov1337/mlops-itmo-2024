import os
import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.option(
    "--final-path",
    type=click.Path(exists=True),
    default="../data/interim/final.csv",
    help="Path to the final CSV file.",
)
@click.option(
    "--features-dir",
    type=click.Path(),
    default="../data/processed",
    help="Directory to save the processed features.",
)
def main(final_path, features_dir):
    final = load_data(final_path)
    final = preprocess_data(final)
    X_train, X_test, y_train, y_test = split_data(final)

    save_data(X_train, X_test, y_train, y_test, features_dir)

    click.echo("Files created successfully.")


def load_data(file_path):
    return pd.read_csv(file_path, index_col="customerID")


def preprocess_data(df):
    df["Leave"] = (df["EndDate"] != "No").astype(int)
    df["EndDate"] = df["EndDate"].replace("No", "2020-02-01")
    df["EndDate"] = df["EndDate"].astype("datetime64[ns]")
    df["BeginDate"] = df["BeginDate"].astype("datetime64[ns]")
    df["Days"] = (df["EndDate"] - df["BeginDate"]).dt.days
    df = df.drop(["BeginDate", "EndDate", "TotalCharges"], axis=1)
    df = df.fillna("No")
    return df


def split_data(df, random_state=1776):
    X = df.drop("Leave", axis=1)
    y = df["Leave"]
    X = pd.get_dummies(X, drop_first=True)
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.1, random_state=random_state
    )
    return X_train, X_test, y_train, y_test


def save_data(X_train, X_test, y_train, y_test, output_dir):
    os.makedirs(output_dir, exist_ok=True)
    X_train.to_csv(os.path.join(output_dir, "X_train.csv"))
    X_test.to_csv(os.path.join(output_dir, "X_test.csv"))
    y_train.to_csv(os.path.join(output_dir, "y_train.csv"))
    y_test.to_csv(os.path.join(output_dir, "y_test.csv"))


if __name__ == "__main__":
    main()
