# -*- coding: utf-8 -*-

import os
from dotenv import load_dotenv
import click
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, f1_score
import mlflow
import mlflow.sklearn

load_dotenv()


@click.command()
@click.option(
    "--x-train-dir",
    type=click.Path(exists=True),
    default="../../data/processed/X_train.csv",
    help="Path to X_train.csv file.",
)
@click.option(
    "--x-test-dir",
    type=click.Path(exists=True),
    default="../../data/processed/X_test.csv",
    help="Path to X_test.csv file.",
)
@click.option(
    "--y-train-dir",
    type=click.Path(exists=True),
    default="../../data/processed/y_train.csv",
    help="Path to y_train.csv file.",
)
@click.option(
    "--y-test-dir",
    type=click.Path(exists=True),
    default="../../data/processed/y_test.csv",
    help="Path to y_test.csv file.",
)
@click.option(
    "--criterion",
    type=click.Choice(["gini", "entropy"]),
    default="gini",
    help="Splitting criterion for random forest.",
)
@click.option(
    "--max-depth", type=int, default=None, help="Maximum depth of the trees in the random forest."
)
@click.option(
    "--min-samples-split",
    type=int,
    default=2,
    help="Minimum samples required to split an internal node in the random forest.",
)
@click.option(
    "--min-samples-leaf",
    type=int,
    default=1,
    help="Minimum samples required to be at a leaf node in the random forest.",
)
def main(
    x_train_dir,
    x_test_dir,
    y_train_dir,
    y_test_dir,
    criterion,
    max_depth,
    min_samples_split,
    min_samples_leaf,
):
    # Read data
    x_train = pd.read_csv(x_train_dir, index_col="customerID")
    x_test = pd.read_csv(x_test_dir, index_col="customerID")
    y_train = pd.read_csv(y_train_dir, index_col="customerID")
    y_test = pd.read_csv(y_test_dir, index_col="customerID")

    # Initialize MLflow tracking
    mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
    mlflow.set_experiment("random_forest_experiment")

    with mlflow.start_run():
        # Create and train the model
        rf = RandomForestClassifier(
            criterion=criterion,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            random_state=42,
        )
        rf.fit(x_train, y_train.values.ravel())

        # Predict on test set
        y_pred = rf.predict(x_test)
        y_pred_proba = rf.predict_proba(x_test)[:, 1]

        # Calculate AUC ROC and F1 score
        auc_roc = roc_auc_score(y_test, y_pred_proba)
        f1 = f1_score(y_test, y_pred)

        # Log parameters and metrics to MLflow
        mlflow.log_params(
            {
                "criterion": criterion,
                "max_depth": max_depth,
                "min_samples_split": min_samples_split,
                "min_samples_leaf": min_samples_leaf,
            }
        )
        mlflow.log_metric("auc_roc", auc_roc)
        mlflow.log_metric("f1_score", f1)

        mlflow.sklearn.log_model(rf, f"model_{mlflow.active_run().info.run_id}")

        mlflow.register_model(
            f"runs:/{mlflow.active_run().info.run_id}/model_{mlflow.active_run().info.run_id}",
            f"model_{mlflow.active_run().info.run_id}",
        )

    click.echo(f"Success. Model AUC ROC: {auc_roc}, F1 Score: {f1}")


if __name__ == "__main__":
    main()
