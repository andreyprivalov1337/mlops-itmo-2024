import os
import click
import pandas as pd
import matplotlib.pyplot as plt


@click.command()
@click.option("--data_dir", default="data/raw", help="Directory containing raw data CSV files")
@click.option("--report_dir", default="reports/figures", help="Directory to save generated plots")
def main(data_dir, report_dir):
    data_dir = os.path.join(os.path.dirname(__file__), "..", data_dir)
    report_dir = os.path.join(os.path.dirname(__file__), "..", report_dir)

    contract_path = os.path.join(data_dir, "contract.csv")
    internet_path = os.path.join(data_dir, "internet.csv")
    personal_path = os.path.join(data_dir, "personal.csv")
    phone_path = os.path.join(data_dir, "phone.csv")

    contract = pd.read_csv(contract_path, index_col="customerID")
    personal = pd.read_csv(personal_path, index_col="customerID")
    internet = pd.read_csv(internet_path, index_col="customerID")
    phone = pd.read_csv(phone_path, index_col="customerID")

    def plot_and_save_histogram(data, column, title, save_path):
        plt.figure(figsize=(12, 6))
        data[column].hist(bins=10)
        plt.title(title)
        plt.savefig(save_path)
        plt.close()

    os.makedirs(os.path.join(report_dir, "contract"), exist_ok=True)
    os.makedirs(os.path.join(report_dir, "personal"), exist_ok=True)
    os.makedirs(os.path.join(report_dir, "internet"), exist_ok=True)
    os.makedirs(os.path.join(report_dir, "phone"), exist_ok=True)

    plot_and_save_histogram(
        contract, "Type", "Payment Type", os.path.join(report_dir, "contract", "type_hist.png")
    )
    plot_and_save_histogram(
        contract,
        "PaperlessBilling",
        "Paperless Billing",
        os.path.join(report_dir, "contract", "paperless_billing_hist.png"),
    )
    plot_and_save_histogram(
        contract,
        "PaymentMethod",
        "Payment Method",
        os.path.join(report_dir, "contract", "payment_method_hist.png"),
    )

    plot_and_save_histogram(
        personal, "gender", "Gender", os.path.join(report_dir, "personal", "gender_hist.png")
    )
    plot_and_save_histogram(
        personal,
        "Partner",
        "Presence of Partner",
        os.path.join(report_dir, "personal", "partner_hist.png"),
    )
    plot_and_save_histogram(
        personal,
        "Dependents",
        "Presence of Dependents",
        os.path.join(report_dir, "personal", "dependents_hist.png"),
    )

    plot_and_save_histogram(
        internet,
        "InternetService",
        "Internet Service Type",
        os.path.join(report_dir, "internet", "internet_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "OnlineSecurity",
        "Presence of Online Security",
        os.path.join(report_dir, "internet", "onlinesecurity_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "OnlineBackup",
        "Presence of Online Backup",
        os.path.join(report_dir, "internet", "onlinebackup_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "DeviceProtection",
        "Presence of Device Protection",
        os.path.join(report_dir, "internet", "deviceprotection_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "TechSupport",
        "Tech Support Contacted",
        os.path.join(report_dir, "internet", "techsupport_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "StreamingTV",
        "Presence of Streaming TV",
        os.path.join(report_dir, "internet", "streamingtv_hist.png"),
    )
    plot_and_save_histogram(
        internet,
        "StreamingMovies",
        "Presence of Streaming Movies",
        os.path.join(report_dir, "internet", "streamingmovies_hist.png"),
    )

    plot_and_save_histogram(
        phone,
        "MultipleLines",
        "Presence of Multiple Lines",
        os.path.join(report_dir, "phone", "multiplelines_hist.png"),
    )

    click.echo("All plots created successfully.")


if __name__ == "__main__":
    main()
