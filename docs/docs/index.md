# churn client documentation!

## Description

Communication operator "Connection.com " wants to learn how to predict the outflow of customers. If it turns out that the user is planning to leave, he will be offered promo codes and special conditions. The operator's team collected personal data about some customers, information about their tariffs and contracts.

## Commands

The Makefile contains the central entry points for common tasks related to this project.

